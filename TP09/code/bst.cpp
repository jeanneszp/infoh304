#include <iostream>
#include <string>
#include <chrono>
#include <thread>
#include <random>
#include <unordered_set>
extern "C" {
#include <UbigraphAPI.h>
}

using namespace std;

class BinarySearchTree
{
private:
	struct node
	{
		node *left;
		node *right;
		node *parent;
		int content;
	};
	node* root;
	node* tree_search(int content);
	node* tree_search(int content, node* location);
	void delete_no_child(node* location);
	void delete_left_child(node* location);
	void delete_right_child(node* location);
	void delete_two_children(node* location);
	
public:
	BinarySearchTree ()
	{
		root=NULL;
	}
	bool isEmpty()
	{
		return root==NULL;
	}
	void insert_element(int content);
	void delete_element(int content);
	void inorder(node* location);
	void print_inorder();
};


void BinarySearchTree::insert_element(int content)
{
	//New node
	node* n = new node();
	n->content = content;
	n->left = NULL;
	n->right = NULL;
	n->parent = NULL;
	
	//For visualization
	int eid,vid;
	this_thread::sleep_for(chrono::milliseconds(100));
	ubigraph_new_vertex_w_id(content);
	ubigraph_set_vertex_attribute(content, "color", "#0000ff");
	ubigraph_set_vertex_attribute(content, "label", to_string(content).c_str());
	
	if(isEmpty())
	{
		root = n;
		ubigraph_set_vertex_attribute(content, "color", "#ff0000");
		this_thread::sleep_for(chrono::milliseconds(100));
		ubigraph_set_vertex_attribute(content, "color", "#0000ff");
		this_thread::sleep_for(chrono::milliseconds(100));
	}
	else
	{
		
		node* pointer = root;
		
		while ( pointer != NULL )
		{
			n->parent = pointer;
			
			if(n->content > pointer->content)
			{
				vid = pointer->content;
				ubigraph_set_vertex_attribute(vid, "color", "#ff0000");
				this_thread::sleep_for(chrono::milliseconds(100));
				ubigraph_set_vertex_attribute(vid, "color", "#0000ff");
				this_thread::sleep_for(chrono::milliseconds(100));
				
				pointer = pointer->right;
				
			}
			else
			{
				vid = pointer->content;
				ubigraph_set_vertex_attribute(vid, "color", "#ff0000");
				this_thread::sleep_for(chrono::milliseconds(100));
				ubigraph_set_vertex_attribute(vid, "color", "#0000ff");
				this_thread::sleep_for(chrono::milliseconds(100));
				
				pointer = pointer->left;
				
			}
		}
		
		if ( n->content < n->parent->content )
		{
			n->parent->left = n;
			this_thread::sleep_for(chrono::milliseconds(200));
			eid = ubigraph_new_edge(n->parent->content, content);
			ubigraph_set_edge_attribute(eid, "oriented", "true");
			
		}
		else
		{
			n->parent->right = n;
			this_thread::sleep_for(chrono::milliseconds(200));
			eid = ubigraph_new_edge(n->parent->content, content);
			ubigraph_set_edge_attribute(eid, "oriented", "true");		
		}
	}
}

void BinarySearchTree::inorder(node* location)
{
	//Pseudo code slide 12 du cours 9
	if (location != NULL)
	{
		inorder (location->left);
		cout << " " << location->content;
		inorder (location->right);
	}
}


void BinarySearchTree::print_inorder()
{
	cout << "Contenu de l'arbre :";
	inorder(root);
	cout << endl;
}

BinarySearchTree::node* BinarySearchTree::tree_search(int content)
{
	return tree_search(content, root);
}

BinarySearchTree::node* BinarySearchTree::tree_search(int content, node* location)
{
	while (location != NULL)
	{
		if (location->content == content)
		{
			return location;
		}
		else if (location->content > content)
		{
			location = location->left;
		}
		else
		{
			location = location->right;
		}
	}
	return NULL;
}

void BinarySearchTree::delete_no_child(node* location)
{
	cout << "Le noeud à supprimer n'a pas d'enfant." << endl;
	if (location->parent == NULL)
		delete location;
	else if (location->content > location->parent->content)
	{
		location->parent->right = NULL;
		delete location;
	}
	else 
	{
		location->parent->left = NULL;
		delete location;
	}
}

void BinarySearchTree::delete_left_child(node* location)
{
	cout << "Le noeud à supprimer a un enfant à gauche." << endl;
}

void BinarySearchTree::delete_right_child(node* location)
{
	cout << "Le noeud à supprimer a un enfant à droite." << endl;
}

void BinarySearchTree::delete_two_children(node* location)
{
	cout << "Le noeud à supprimer a deux enfants." << endl;
}

void BinarySearchTree::delete_element(int content)
{
	node *location = tree_search(content,root);
	if (location != NULL)
	{
		if (location->right != NULL && location->left != NULL)
		{
			delete_two_children(location);
		}
		else if (location->right != NULL)
		{
			delete_right_child(location);
		}
		else if (location->left != NULL)
		{
			delete_left_child(location);
		}
		else
		{
			delete_no_child(location);
		}
	}
	else
	{
		cout << "Le contenu recherché ne se trouve pas dans l'arbre." << endl;
	}
}


int main()
{
	ubigraph_clear();
	unordered_set<int> vertices;
	int vertex;
	BinarySearchTree bst;
	
	/*Partie du code pour insérer des nombres aléatoires
	random_device rd;
	mt19937 gen(rd());
	uniform_int_distribution<int> r(0, 1000);
	
	for ( int i=1; i<=10; i++ )
	{
		vertex = r(gen);
		if ( vertices.count(vertex) == 0 )
		{
			vertices.insert(vertex);
			bst.insert_element(vertex);
		}
		else
			i--;
	}*/
	//Si on insère des nombres de plus en plus grands ou de plus en plus petits, l'arbre est complètement déséquilibré
	//Pour les nombres suivants, l'arbre est complètement équilibré. 
	bst.insert_element(13);
	/*bst.insert_element(10);
	bst.insert_element(15);
	bst.insert_element(3);
	bst.insert_element(12);
	bst.insert_element(14);
	bst.insert_element(20);
	bst.insert_element(2);
	bst.insert_element(8);
	bst.insert_element(11);*/
	
	bst.print_inorder();
	/*bst.delete_element(10);
	bst.delete_element(2);
	bst.delete_element(12);
	bst.delete_element(24);*/
	bst.delete_element(13);
	bst.print_inorder();
	
	return 0;
}
