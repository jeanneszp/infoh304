# Enoncés et code de base pour les exercices du cours INFOH304

Ce dépôt git contient les énoncés et le code de base pour les séances d'exercices du cours INFOH304.

Vous pouvez directement télécharger les fichiers depuis ce site ou cloner le dépôt avec la commande
`git clone git@gitlab.com:jeremieroland/infoh304.git`.

Pour plus d'efficacité, il est néanmoins recommandé de créer un fork de ce dépôt, ce qui vous permettra d'y sauvegarder votre travail (vous ne pouvez pas le faire sur le dépôt original auquel vous n'avez accès qu'en lecture). Voici la démarche complète.

## Prérequis

Pour commencer, il faut avoir installé et configuré git.
- L'installation sur la machine virtuelle se fait via la commande `sudo apt install git` (vous devrez entrer le mot de passe "ulb").
- Renseignez votre nom d'utilisateur et votre adresse email git via les commandes suivantes (remplacez $EMAIL_ADDRESS et $USERNAME par vos identifiants GitLab)
```
git config --global user.email $EMAIL_ADDRESS
git config --global user.name $USERNAME
```
- Pour plus de facilité, créez une clé SSH et ajoutez-la dans votre compte GitLab (voir vidéo guidance)

## Création du fork sur GitLab

- Rendez-vous sur la page d'accueil de ce dépôt ([lien](https://gitlab.com/jeremieroland/infoh304))
- Si vous n'êtes pas encore connecté avec votre compte, faites-le en cliquant en haut à droite sur "Sign in / Register"
- Cliquez sur le bouton "Fork" en haut à droite
- Cliquez sur "Select" en-dessous de votre nom

## Clonage du fork sur votre ordinateur
- Depuis un terminal, naviguez vers le dossier où vous voudriez cloner le projet
```
cd nom/du/dossier
```
- Clonez le fork localement (remplacer $USERNAME par votre identifiant GitLab)
```
git clone git@gitlab.com:$USERNAME/infoh304.git
```
- Ajoutez le projet original comme "remote upstream" pour pouvoir tirer les mises à jour
```
cd infoh304
git remote add upstream git@gitlab.com:jeremieroland/infoh304.git
```

## Utilisation
- Au début de chaque TP, vous pouvez récupérer les mises à jour (énoncés et code de base du nouveau TP) depuis le dépôt original avec la commande
```
git pull --no-edit upstream master
```
- Vous pouvez alors travailler localement sur le TP en modifiant ou ajoutant des fichiers
- A la fin du TP (ou au fur et à mesure de votre travail), vous pouvez sauvegarder votre travail dans votre fork du dépôt INFOH304, par exemple via les commandes
```
git add . 
git commit -a -m "Travail sur le TP04"
git push
```


## Remarque
La commande `git pull --no-edit upstream master` incluant un merge depuis un autre dépôt, il se peut qu'elle crée des conflits. Cela ne devrait néanmoins pas être le cas car la plupart des mises à jour du dépôt original ne consisteront qu'en l'ajout de nouveaux fichiers, sans modification des fichiers existants.

Pour éviter tout souci, assurez-vous néanmoins que vos fichiers locaux soient bien à jour par rapport à votre fork avant de réaliser le pull depuis le dépôt original, en particulier
- Si vous avez modifié les fichiers localement, vous devez pousser les changements vers votre fork sur GitLab via les commandes `git add [...]`, `git commit [...]`, et `git push` comme indiqué ci-dessus (sinon votre remote sera en retard sur vos fichiers locaux)
- Si vous avez travaillé sur une autre machine à partir de laquelle vous avez modifié les fichiers de votre fork sur GitLab, récupérez d'abord les changements avec un `git pull` (sinon vos fichiers locaux seront en retard sur votre remote)

Notez qu'en travaillant de la sorte, les fichiers que vous avez modifiés lors de votre travail ne seront pas écrasés par la version dans le dépôt original lorsque vous effectuerez la commande `git pull --no-edit upstream master`, et même en cas d'erreur de manipulation, ce n'est pas grave puisque vous pourrez trouver les anciennes versions des fichiers sur votre fork.

